from .models import Delivery
from rest_framework import serializers
from django_grpc_framework import proto_serializers
import deliveryservice_pb2


class DeliverySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Delivery
        fields = ['alamat', 'kota', 'metode', 'waktu', 'harga']

class DeliveryProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Delivery
        proto_class = deliveryservice_pb2.Delivery
        fields = ['id', 'alamat', 'kota', 'metode', 'waktu', 'harga']