from django.db import models

class Delivery(models.Model):
    KOTA = [
    ('JA', 'Jakarta'),
    ('BO', 'Bogor'),
    ('DE', 'Depok'),
    ('TA', 'Tangerang'),
    ('BE', 'BEKASI'),
    ]
    METODE = [
        ('GOS', 'Go Send'),
        ('JNE', 'JNE'),
        ('JNT', 'J&T')
    ]
    WAKTU = [
        ('PA', 'Pagi'),
        ('SI', 'Siang'),
        ('SO', 'Sore')
    ]
    alamat = models.TextField(blank=True, null=True)
    kota = models.CharField(
        blank=True, 
        null=True,
        max_length=2,
        choices=KOTA,
        default='JA',
    )
    metode = models.CharField(
        blank=True, 
        null=True,
        max_length=3,
        choices=METODE,
        default='GOS'
    )
    waktu = models.CharField(
        blank=True, 
        null=True,
        max_length=2,
        choices=WAKTU,
        default='PA'
    )
    harga = models.IntegerField(blank=True, null=True,)
