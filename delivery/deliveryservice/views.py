from django.shortcuts import render
from .models import Delivery
from .serializers import DeliverySerializer
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import ParseError
import requests
from requests.structures import CaseInsensitiveDict


class DeliveryView(APIView):
    def get(self, request):
        try:
            list_delivery = Delivery.objects.all()
            serializer = DeliverySerializer(list_delivery, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
            url = "https://api.logit.io/v2"
            headers = CaseInsensitiveDict()
            headers["ApiKey"] = "cc6ea62c-7b13-4cea-9a5d-86410e70dbc1"
            headers["Content-Type"] = "application/json"
            headers["LogType"] = "error"
            data = '{"message":"'+str(e)+'","source":"Logging Service","function":"get_all_logs"}'
            resp = requests.post(url, headers=headers, data=data)

    def post(self, request):
        try:
            kota=request.data["kota"]
            if(kota=="JA"):
                request.data["harga"] = 10000
            elif(kota=="BO"):
                request.data["harga"] = 15000
            elif(kota=="DE"):
                request.data["harga"] = 12500    
            elif(kota=="TA"):
                request.data["harga"] = 13000
            else:
                request.data["harga"] = 14000
            serializer = DeliverySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()

                params = "{}".format(kota)
                url = "https://api.logit.io/v2"
                headers = CaseInsensitiveDict()
                headers["ApiKey"] = "cc6ea62c-7b13-4cea-9a5d-86410e70dbc1"
                headers["Content-Type"] = "application/json"
                headers["LogType"] = "default"
                data = '{"message": "Request harga ke Kota '+params+'","source":"Service Delivery","function":"post"}'
                resp = requests.post(url, headers=headers, data=data)

                return Response(serializer.data, status=status.HTTP_200_OK)
            raise ParseError(
				{
					"message": "Review request body is not valid",
					"data": serializer.data
				}
			)
        except Exception as e:
            url = "https://api.logit.io/v2"
            headers = CaseInsensitiveDict()
            headers["ApiKey"] = "cc6ea62c-7b13-4cea-9a5d-86410e70dbc1"
            headers["Content-Type"] = "application/json"
            headers["LogType"] = "error"
            data = '{"message":"'+str(e)+'","source":"Logging Service","function":"get_all_logs"}'
            resp = requests.post(url, headers=headers, data=data)