import deliveryservice_pb2_grpc
from deliveryservice.services import DeliveryService

def grpc_handlers(server):
    deliveryservice_pb2_grpc.add_DeliveryControllerServicer_to_server(DeliveryService.as_servicer(), server)