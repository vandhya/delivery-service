from django.urls import path

from . import views

app_name = 'deliveryservices'
urlpatterns = [
    path('api/', views.DeliveryView.as_view(), name="delivery-view"),
]