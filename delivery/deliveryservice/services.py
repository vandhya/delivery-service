import grpc
from google.protobuf import empty_pb2
from django_grpc_framework.services import Service
from .models import Delivery
from .serializers import DeliveryProtoSerializer


class DeliveryService(Service):
    def List(self, request, context):
        deliveries = Delivery.objects.all()
        serializer = DeliveryProtoSerializer(deliveries, many=True)
        for msg in serializer.message:
            yield msg

    def Create(self, request, context):
        serializer = DeliveryProtoSerializer(message=request)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer.message

    def get_object(self, pk):
        try:
            return Delivery.objects.get(pk=pk)
        except Delivery.DoesNotExist:
            self.context.abort(grpc.StatusCode.NOT_FOUND, 'Delivery:%s not found!' % pk)

    def Retrieve(self, request, context):
        delivery = self.get_object(request.id)
        serializer = DeliveryProtoSerializer(delivery)
        return serializer.message

    def Update(self, request, context):
        post = self.get_object(request.id)
        serializer = DeliveryProtoSerializer(post, message=request)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer.message

    def Destroy(self, request, context):
        delivery = self.get_object(request.id)
        delivery.delete()
        return empty_pb2.Empty()