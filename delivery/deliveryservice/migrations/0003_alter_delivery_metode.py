# Generated by Django 3.2.13 on 2022-05-27 07:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deliveryservice', '0002_auto_20220527_1408'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delivery',
            name='metode',
            field=models.CharField(blank=True, choices=[('GOS', 'Go Send'), ('JNE', 'JNE'), ('JNT', 'J&T')], default='GOS', max_length=3, null=True),
        ),
    ]
