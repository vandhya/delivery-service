# Generated by Django 3.2.13 on 2022-05-27 06:38

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alamat', models.TextField()),
                ('kota', models.CharField(choices=[('JA', 'Jakarta'), ('BO', 'Bogor'), ('DE', 'Depok'), ('TA', 'Tangerang'), ('BE', 'BEKASI')], default='JA', max_length=2)),
                ('metode', models.CharField(choices=[('REG', 'Reguler'), ('EKO', 'Ekonomi'), ('INS', 'Instan')], default='REG', max_length=3)),
                ('waktu', models.DateTimeField(verbose_name='waktu pengiriman')),
                ('harga', models.IntegerField()),
            ],
        ),
    ]
