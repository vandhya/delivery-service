import grpc
import deliveryservice_pb2, deliveryservice_pb2_grpc

with grpc.insecure_channel('localhost:50051') as channel:
    stub = deliveryservice_pb2_grpc.DeliveryControllerStub(channel)
    print('----- Create -----')
    response = stub.Create(deliveryservice_pb2.Delivery(alamat='test', kota='JA', metode='GOS', waktu='PA', harga=1))
    print(response, end='')
    print('----- List -----')
    for delivery in stub.List(deliveryservice_pb2.DeliveryListRequest()):
        print(delivery, end='')
    print('----- Retrieve -----')
    response = stub.Retrieve(deliveryservice_pb2.DeliveryRetrieveRequest(id=response.id))
    print(response, end='')
    print('----- Update -----')
    response = stub.Update(deliveryservice_pb2.Delivery(id=response.id, alamat='testis', kota='BO', metode='JNE', waktu='SI', harga=2))
    print(response, end='')
    print('----- Delete -----')
    stub.Destroy(deliveryservice_pb2.Delivery(id=response.id))